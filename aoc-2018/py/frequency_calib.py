import itertools


def read_frequencies(path_to_file):
    with open(path_to_file, 'r') as f:
        frequency_changes = [int(line) for line in f.readlines()]
    return frequency_changes


def solution_part_1(freq):
    return sum(freq)


def solution_part_2(freq):
    current_frequency = 0
    seen_frequencies = {0}
    for frequency in itertools.cycle(freq):
        current_frequency += frequency
        if current_frequency in seen_frequencies:
            return current_frequency
        seen_frequencies.add(current_frequency)


file_path = "inputs/1.txt"
frequencies = read_frequencies(file_path)
print("Solution Part #1 : ", solution_part_1(frequencies))
print("Solution Part #2 : ", solution_part_2(frequencies))
