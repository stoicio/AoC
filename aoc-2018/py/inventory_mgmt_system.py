from collections import Counter


def read_lines(path_to_file):
    with open(path_to_file, 'r') as f:
        data = [line.rstrip() for line in f]
    return data


def count_with_exactly_n(data, n):
    count = 0

    for datum in data:
        counts = Counter(datum)
        found_n = False
        for element in counts:
            v = counts[element]
            if v == n and not found_n:
                count += 1
                found_n = True
    return count


def solution_part_1(data):
    count_2 = count_with_exactly_n(data, 2)
    count_3 = count_with_exactly_n(data, 3)
    return count_3 * count_2


def word_edit_distance(a, b):
    distance = 0
    common = []
    for i in range(0, len(a)):
        if a[i] != b[i]:
            distance += 1
        else:
            common.append(a[i])
    return distance, ''.join(common)


def solution_part_2(data):
    min_distance = 27  # Maxlength of input is 26
    candidate = None
    for i in range(0, len(data)):
        for j in range(0, len(data)):
            if i != j:
                distance, common = word_edit_distance(data[i], data[j])
                if distance < min_distance:
                    candidate = common
                    min_distance = distance
    return candidate


file_path = "inputs/2.txt"
box_ids = read_lines(file_path)
print("Solution Part 1 : ", solution_part_1(box_ids))
print("Solution Part 2 : ", solution_part_2(box_ids))
